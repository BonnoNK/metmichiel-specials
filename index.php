<?php
/**
 * Copyright 2014 Bonno Nachtegaal-Karels
 * Version: 0.1
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);

date_default_timezone_set('Europe/Amsterdam');
$loc = setlocale(LC_ALL, 'nl_NL.utf8', 'nl_NL');

header('Content-type: application/rss+xml');

if (file_exists('cache.xml')) {
	echo file_get_contents('cache.xml');
	die;
}

$radio_url = 'http://metmichiel.3fm.nl/specials';
$html = file_get_contents($radio_url);
$config = array(
	'output-xhtml'   => true,
);

// Tidy
$tidy = new tidy;
$tidy->parseString($html, $config, 'utf8');
$tidy->cleanRepair();
$html = (string)$tidy;

$doc = new DOMDocument();
$doc->recover = true;
$doc->strictErrorChecking = false;
$doc->formatOutput = true;
$doc->loadHTML($html);
$specials = array();

//echo '<pre>';
//var_dump($doc->saveHTML());

$nr = 0;
$time = new DateTime('now');
foreach($doc->getElementsByTagName('div') as $div) {
	if ( stripos($div->getAttribute('class'), 'column-wrapper8') !== false) {
		foreach($div->getElementsByTagName('div') as $specialoverview_div) {
			if (stripos($specialoverview_div->getAttribute('class'), 'box grid_8 overzicht') !== false ) {
				$special = array();
				$nr++;
				foreach($specialoverview_div->getElementsByTagName('div') as $special_div) {
					if (stripos($special_div->getAttribute('class'), 'boxheader boxheader_top') !== false ) {
						//title
						$special['title'] =  trim($special_div->nodeValue);
					}
					elseif ( stripos($special_div->getAttribute('class'), 'boxcontent padded') !== false ) {
						foreach($special_div->getElementsByTagName('div') as $special_content) {
							if (stripos($special_content->getAttribute('class'), 'article') !== false ) {
								$introP = $special_content->getElementsByTagName('p')->item(0);
								if(is_object($introP)) {
									//intro
									$special['intro'] = trim(  str_replace('&nbsp;', ' ', strip_tags($introP->nodeValue)) );
								}
								$fileDiv = $special_content->getElementsByTagName('div')->item(0);
								if(is_object($fileDiv)) {
									//file
									$file = trim($fileDiv->getAttribute('data-url'));
									if ($file > '') {
										/*$headers = get_headers($file, 1);
										if(array_key_exists('Location', $headers)) {
											if (is_array($headers['Location'])) {
												$headers['Location'] = $headers['Location'][0];
											}
											parse_str(html_entity_decode(parse_url($headers['Location'], PHP_URL_QUERY)), $redirectArr);
											if(array_key_exists('ns_url', $redirectArr)) {
												$file = $redirectArr['ns_url'];
												$headers = get_headers($file, 1);
											}
										}*/
										$special['file'] = $file;
										$special['content-length'] = 15000000;//$headers['Content-Length'][1];
									}
								}
							}
							
						}
					}
				}
				$special['pubDate'] = $time->modify("-1 week")->format(DateTime::RFC2822);
				if (array_key_exists('file', $special) && $special['file'] > '') {
					$specials[] = $special;
				}
			}
		}
	}
}

$web_url = 'http://metmichiel.3fm.nl/specials';
$podcast_title = 'MetMichiel - Specials';
$author = 'Michiel Veenstra';
$description = 'Michiel Veenstra draait de fijnste liedjes, het beste van nu en zoekt de mooiste verhalen achter de liedjes, de albums of de artiesten.';
print '<?xml version="1.0" encoding="UTF-8"?>';
?>

<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
  <channel>
		<title><?=$podcast_title?></title>
		<link><?=$web_url?></link>
		<language>nl-NL</language>
		<copyright><?=date('Y')?> NTR - MetMichiel</copyright>
		<category>Music</category>
		<itunes:subtitle></itunes:subtitle>
		<itunes:author><?=$author?></itunes:author>
		<itunes:owner>
			<itunes:name><?=$author?></itunes:name>
			<itunes:email>michiel@3fm.nl</itunes:email>
		</itunes:owner>
		<itunes:image href="http://bonno.bo-ma.nl/divers/metmichiel-specials/metmichiel.jpg"></itunes:image>
		<itunes:summary><![CDATA[<?=$description?>]]></itunes:summary>
		<description><![CDATA[<?=$description?>]]></description>
		<image>
			<url>http://bonno.bo-ma.nl/divers/metmichiel-specials/metmichiel.jpg</url>
			<title><?=$podcast_title?></title>
			<link><?=$web_url?></link>
		</image>
<?php
foreach ($specials as $special): ?>
			<item>
				<title><![CDATA[<?=htmlentities($special['title'])?>]]></title>
				<itunes:author><?=$author?></itunes:author>
				<itunes:subtitle></itunes:subtitle>
				<itunes:summary><![CDATA[<?=$special['intro'] ?>]]></itunes:summary>
				<description><![CDATA[<?=$special['intro']?>]]></description>
				<enclosure url="<?=$special['file']?>" length="<?=$special['content-length']?>" type="audio/mp3" />
				<guid><?=$special['file']?></guid>
				<pubDate><?=$special['pubDate']?></pubDate>
				<itunes:duration>15:00</itunes:duration>
			</item>
<?php endforeach;?>
	</channel>
</rss>
