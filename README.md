MetMichiel Specials Podcast
===========================

A simple (PHP) script that parses the content from http://metmichiel.3fm.nl/specials and transforms it to a RSS file so it can be read by podcast clients.
This way you have the full list of specials available in your reader instead of only the last 7 specials from Michiel Veenstra.